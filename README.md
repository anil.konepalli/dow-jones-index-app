# Dow Jones Index Data Project

The application will help you to create a index with the data provided from below link and gives you APIs to integrate with any application.

http://archive.ics.uci.edu/ml/datasets/Dow+Jones+Index#

### Tech stack decision: 
- MongoDB - The schema is going to be dynamic for most of the times for projects like this hence NoSql database is best fit. 
- Spring Boot 
  - Spring provides a lot of features such as embedded container, docerization support , and plug and play features for many defferent technologies and frameworks.
  - we can simply focus on building the application rathar than spending on setting up the project dependencies and tools.
  - Also, it provides support for multi threading / processing. 
  - Batch processing support 
- Docker: Helps us to manage and scale independent services.

#### Flow Diagram

[Design Document](https://viewer.diagrams.net/?tags=%7B%7D&highlight=0000ff&edit=_blank&layers=1&nav=1&title=final.drawio#R5Vpbk6I4FP41PmpxBx%2B1L05tzey61Vs73U9TEdLKbCBsiK32r98ACdeoaIPaOy9KDiGE79zPyUC%2FC7YzAqLVN%2BxBNNAUbzvQ7weapuq6yf4Syo5THEXPKEvie5xWEJ78d8iJCqeufQ%2FGlYkUY0T9qEp0cRhCl1ZogBC8qU57xaj61ggsYYPw5ALUpH73PbrKqI6pFPQv0F%2BuxJtVhd8JgJjMCfEKeHhTIukPA%2F2OYEyzq2B7B1GCnsAle%2B5xz918YwSGtM0Dq2dDMabTL44zCdXNy%2FPs7xAMjXG2zBtAa%2F7FfLd0JyAgeB16MFlFGejTzcqn8CkCbnJ3w7jOaCsaIDZS2eUrDinnouqwcXOXfONvkFC4LZH4rmcQB5CSHZvCZWZocQQ3BQMsm9NWJfBtATXgTF%2FmaxW4sAsOzSkwKTcME7%2Brq02YNFMCkzbuAKb7%2BPfZbKfstu%2FbRbT4OXv%2F05wOVauBCvSYOvEhJnSFlzgE6KGgTqu4FXO%2BYhxxtH5CSnccLrCmuIolg4zsnvnz6eAlGYxMMbzflm%2Fe78qjOSQ%2B%2B3hIOLHMmHHOmOQr9qodJ8V4TVx4SIiE%2FQJkCemBiaaczwQiQP236ka6F3UheDcs6pYwssJCSETfkEi%2B1ZuB0J0bRu2YQbDUvmDRjAYsjz7DQ1OeKCaJ690PknocJLCIMVpTOCGuwCqhFiODDWNK8D%2B589Yk2ALkL0M2QPCVdmSL7aqAqnZTQFVLwoo8fOieFfpxCW1CVQKbxS9RMi%2FYLpNYb%2FSK8MZdAUJHwZrFY0MPu%2BsghUzKth7MgGFrI7OCcx5slXA29SbMRm8e0LmuBxxpZtUJqsatuUG9pRc09jD%2FMm5Qpi0WohyaCoetf9dY3BjGKWgTNkEdR9sUOXGfXS2T%2F0m8C91BIqnZggsi7kwBdVfszpxgF8axHy7FHPYN2XuziVcxm13YRaeusYbEMBpmU2OdvuyiedwsJt%2Fns5TwK1hANMexT32c%2BIsFphQHDBkxYcIdCcVRW9vJ4AQIQfQjYLkzmxlhP2Rq%2BPDGQI750wRTwF%2BpdMMHuxZA5fCWUywJF7qwm1IuqDJ9ewSRz359Jtlb9r%2BOEAbeTUQMXTivWoSgSBRBluR2EiHIfZdxDd9V9zRw69OyK2PDF%2BGp2HXhxJKB8GEduifDaemfsmT3Wv5JpNolhZk9%2FMUIw9Sz1FTnf6o0eTZzRaUxrxrwDZWRojjVkM%2B28ik3EvKp9qeI%2BcQ2Szo1%2F%2BPpV1MqQ1JM6U%2Bp5MF3ixrUDQVlw3FHYVkOvQgKDFuEyyVmOBJe9FfYalZwmgYv9CZJq4WNXARY3uJWga6au7oEn12%2B5YGCyiygVo4URoZxMFhIBnXj9wHbpksN5cEqnKwIJ2itTR1%2FwzyRzVKGpe2pPIklsg%2FiTxXC0VzI3uNrxUKZMW8sxCQB7ErTUt2JT9iwYRzelybfVyHk2Q4Kkc958AEtaEZbXWtBl9J8rkp9XAuOevjb0harZnD1emusrbZYVm2heqZ8I9riXEBZjKbLeIpIWsxSeIWrpjrMPdKab04DnzuMcCLTIQ4TN%2FzqI1QjidK9C8NU%2FBteP%2FA9L425ZeHYYZXswKE3611Nd65qkkKL3lsnu1nvypnzHS5%2BHdY4yjG%2BqBK70x9fmg3EnC%2B%2FzSefhC99BMWmade7PJIDIZfVIuF%2BpCYuOdGjKZMoQgzVFI0kV0xOKFHghwzyz8HLDlhn1TmXm59yo37ck55J6zUtyv3xxg8QSPGNmdsWoBTi3ABAAtN%2Bu1PP8SQty%2Fzzq8d2cjS7r3xIUDmx2%2BXs6XZ9w%2BES30%2FbNrLObEF7gIIFiPdJepNJB4WjvW2qu3dZn1%2FmR9R6NtVdQVJ2VK2jzmXJhLXj5m1YtlZau18gDnTRqpos6aFd0rSpLc5ySpPU1MqdmbueVik%2B9%2BjbhbLBWiVUrxc422aDtl5dqG3t5IwMTCoJWov6aS%2BSUDTvFEWtVDGsvJt3alHu1A7EKYp8Ibkya3JVP2%2FXWq7qVQbF6kiu2LA4iZ5NLw706w%2F%2FAQ%3D%3D)


### Improvements can be done if time permits such as
- Writing more Unit and integration test cases and improving the code coverage.
- Writing contract test cases. 
- Validations on the APIs.
- Separating the batch process form the main application container.
- Authentication and Autherization
- Swagger integration
- If there is any service discovery server, need to discovery clint discovery confguraiton. 


### These are the three main functionalities implemented as part of this project. 

1.  upload a bulk data set
  - Assumptions
    - all users will be uploading file to the same database and collection. 
    - There is no user specific data stored in the index.
    - Technical Approach:
      - The Design document will give a better idea on how it works. 
        - User uploads a file 
        - Rest API saves the file and initiates the batch process.
        - Batch process will start asynchronously and process the data and push it to database.
    - Improvements can be done 
        - We can containerize the batch process to handle multiple requests independently. If they are huge datasets this would help processing with out blocking main application. 
        - Once upload upload request comes request will be pushed to queue to trigger the process in the background using multiple containers.
        - Once file is processed, notify the user using message queue (microservice)
        - User will be notified once file processing is done. 

    ##### API Reference

    ##### upload API

     ```http
    POST /api/index/upload
    
    Conect-type: text/csv 

    upload the csv file.   
    
    ```


2. Query for data by stock ticker (e.g. input: AA, would return 12 elements if the only data uploaded were the single data set above)
  - Assumptions: 
    - Since the above statement states that the expected count for "AA" stock ticker is 12, I have assumed that we want to fetch first quarter date where as 2nd quarter data has 13 records.
  - Technical Approach:
    - I have create an API to default to quarter 1 unless user sends quater param along with the stock ticker.
    - If user sends quarter as 0 API will fetch all the records for the matchig stock ticker.
    - If there is no matching record will thorw Exception.
         ##### API Reference
         #### Get All the Index Data matching the conditions.
    
        ```http
          GET /api/index?stock=AA

          GEt /api/index?stock=AA&quarter=1 (quarter param  is optional and by default it is 1)
        ```


3. Add a new record.

- Assumption
  - The data shows that it weekly data for each stock ticker basically the date and the stock ticker can't be duplicated also the quarter. Hence, I have used stock ticker, Date, quarter as unique combination to update or insert the data. 
    - Technical approach:
      - If the sends duplicated record which is already there it upserts the data.
      - If there is no combination matches it will create a new entry. 

      ##### API Reference
    
      ##### Create a Index Data record in DB
    
    ```http
    POST /api/index
    Conect-type: application/json     
    Body 
     {
      "quarter": 1,
      "stock": "AA",
      "date": "1/7/2011",
      "open": 15.92,
      "high": 16.72,
      "low": 15.78,
      "close": 16.42,
      "volume": 239655616,
      "percent_change_price": 3.79267,
      "next_weeks_open": 16.71,
      "next_weeks_close": 15.97,
      "percent_change_next_weeks_price": -4.42849,
      "days_to_next_dividend": 26,
      "percent_return_next_dividend": 0.182704
     }
    ```
    
      The API will take the Index Data Request as Input and returns Response.

    
## Installation and execution

clone the project

```bash
 git clone https://gitlab.com/anil.konepalli/dow-jones-index-app.git
```


## Run without setting up the project locally.

Clone the project

```bash
  git clone https://gitlab.com/anil.konepalli/dow-jones-index-app.git
```

Go to the project directory

```bash
  cd dow-jones-index-app
```

run using docker compose

```bash
  docker-compose up -d
```

It will start up the below apps

```bash
  mongodb:27017
```

```bash
 mongo-express:8081
```

```bash
 index-app:8080
```
Incase if you are not able to access please use 

```bash
 docker logs <container name>
```

## Setup the project in IDE.

Clone the project

```bash
  git clone https://gitlab.com/anil.konepalli/dowjones-index.git
```

Setup the project in IDE 
- Required tools:
  - Maven dependencies 
  - IDE 
  - Java 
  - Docker (mongoDB)
    

