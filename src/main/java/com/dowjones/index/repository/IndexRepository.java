package com.dowjones.index.repository;

import com.dowjones.index.domain.IndexData;
import com.dowjones.index.dto.IndexDataRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IndexRepository extends MongoRepository<IndexData, String>, CustomIndexRepository {

    Optional<List<IndexData>> findByStock(String stock);

    Optional<List<IndexData>> findByStockAndQuarter(String stock,Integer quarter);

    List<IndexData> findByQuarterAndStockOrderByCreatedTimestamp(Integer quarter, String stock);

    @Query("{ 'quarter' : ?0 , 'stock', ?1 , 'date': ?2 }")
    Optional<IndexData> findByQuarterStockAndDate(IndexDataRequest indexData);



}
