package com.dowjones.index.repository;

import com.dowjones.index.domain.IndexData;

public interface CustomIndexRepository {

    IndexData updateUsingFindAndReplace(IndexData request);

    IndexData updateOrSave(IndexData indexData);
}
