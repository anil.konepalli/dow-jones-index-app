package com.dowjones.index.repository;

import com.dowjones.index.domain.IndexData;
import lombok.AllArgsConstructor;
import org.springframework.data.mongodb.core.FindAndReplaceOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
@AllArgsConstructor
public class CustomIndexRepositoryImpl implements CustomIndexRepository {

    private final MongoTemplate mongoTemplate;

    public IndexData updateUsingFindAndReplace(IndexData request) {
        Query query = new Query().addCriteria(
                Criteria.where("quarter").is(request.getQuarter())
                        .and("date").is(request.getDate())
                        .and("stock").is(request.getStock())
        );
        FindAndReplaceOptions options = new FindAndReplaceOptions().upsert().returnNew();

        return mongoTemplate.findAndReplace(query, request  , options, IndexData.class, IndexData.class);
    }

    public IndexData updateOrSave(IndexData indexData) {
        return updateUsingFindAndReplace(indexData);
    }

}
