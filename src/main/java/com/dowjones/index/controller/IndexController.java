package com.dowjones.index.controller;

import com.dowjones.index.dto.IndexDataRequest;
import com.dowjones.index.dto.IndexDataResponse;
import com.dowjones.index.exceptions.ResourceNotFoundException;
import com.dowjones.index.service.CsvService;
import com.dowjones.index.service.IndexServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/index")
@RequiredArgsConstructor
public class IndexController {


    private final IndexServiceImpl indexService;
    private final CsvService csvService;

    @PostMapping
    public ResponseEntity<IndexDataResponse> createIndexData(@RequestBody IndexDataRequest indexDataRequest) {
        return ResponseEntity.status(HttpStatus.CREATED).body(indexService.save(indexDataRequest));
    }

    @PostMapping("/upload")
    public ResponseEntity<String> uploadIndexData(@RequestParam("file") MultipartFile file) {
        String message;
        if (csvService.hasCSVFormat(file)) {
            try {
                indexService.uploadFile(file);

                message = MessageFormat.format("Uploaded the {} successfully, will start file processing asynchronously  " ,file.getOriginalFilename());
                return ResponseEntity.status(HttpStatus.OK).body(message);
            } catch (Exception e) {
                message = MessageFormat.format( "Could not upload the file: {} error message {}", file.getOriginalFilename(), e.getMessage());
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
            }
        }
        message = "Please upload a csv file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);

    }

    @GetMapping
    public ResponseEntity<List<IndexDataResponse>> findIndexData(@RequestParam String stock,@RequestParam("quarter") Optional<Integer> quarter) throws ResourceNotFoundException {
             return ResponseEntity.status(HttpStatus.OK).body(indexService.findByStockAndQuarter(stock,quarter));
    }



}
