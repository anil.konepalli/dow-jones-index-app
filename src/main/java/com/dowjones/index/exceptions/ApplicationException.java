package com.dowjones.index.exceptions;

public class ApplicationException extends Exception{

    public ApplicationException(String message){
        super(message);
    }
}
