package com.dowjones.index.exceptions;


public class FileParsingException extends Exception{

    private static final long serialVersionUID = 1L;

    public FileParsingException(String message){
        super(message);
    }
}
