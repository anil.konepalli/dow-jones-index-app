package com.dowjones.index.batch;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@EnableAsync
@Slf4j
public class IndexJobLauncher {

    private final Job job;

    private final JobLauncher jobLauncher;

    @Value("${index.csv.processingfolder}")
    private String processingDir;

    IndexJobLauncher(Job job, JobLauncher jobLauncher) {
        this.job = job;
        this.jobLauncher = jobLauncher;
    }

    @Async
    public void launchFileToJob(String fileName) throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobInstanceAlreadyCompleteException, JobRestartException {
        log.info("Starting job");
        String excelFilePath = String.format("%s/%s", processingDir,fileName);

        JobParameters params = new JobParametersBuilder()
                .addLong("jobId",System.currentTimeMillis())
                .addDate("currentTime",new Date())
                .addString("excelPath",excelFilePath)
                .toJobParameters();
        jobLauncher.run(job, params);

        log.info("Stopping job");
    }

}
