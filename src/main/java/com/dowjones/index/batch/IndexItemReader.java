package com.dowjones.index.batch;

import com.dowjones.index.domain.IndexData;
import com.dowjones.index.service.CsvService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Files;
import java.util.List;

@Component
@Slf4j
public class IndexItemReader extends CsvService implements ItemReader<IndexData> , StepExecutionListener {

    private int index = 0;
    private List<IndexData> indexDataList;


    /**
     * {@inheritDoc}
     */
    @Override
    public IndexData read() {


        IndexData indexData = null;
        log.info("Thread {}, index  {}",Thread.currentThread().getName(),index);
        if(!indexDataList.isEmpty() && index < indexDataList.size()) {
                indexData = indexDataList.get(index);
                index++;
        }
        return indexData;
    }


    @Override
    public void beforeStep(StepExecution stepExecution) {
        String excelFilePath = stepExecution.getJobParameters().getString("excelPath");
        log.info("Before the step {}",excelFilePath);
        try {

            String path = new ClassPathResource(excelFilePath).getPath();
            File file = new File(path);
            // read data in file
            indexDataList = convertCsvtoIndexData(Files.newInputStream(file.toPath()));

            if(indexDataList.isEmpty()) {
                log.info("Empty File");

            }
        } catch (Exception e) {
            log.warn("Cannot read the excel file: {}", e.getMessage());
        }
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        return null;
    }
}
