package com.dowjones.index.batch;

import com.dowjones.index.batch.listeners.JobCompletionListener;
import com.dowjones.index.domain.IndexData;
import com.dowjones.index.batch.processors.IndexItemProcessor;
import com.dowjones.index.batch.validators.IndexJobParametersValidator;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersValidator;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.CompositeJobParametersValidator;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.data.MongoItemWriter;
import org.springframework.batch.item.data.builder.MongoItemWriterBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Collections;

/**
 * Configuration for batch
 */
@EnableBatchProcessing
@Configuration
public class BatchConfiguration extends DefaultBatchConfigurer {

    public final JobBuilderFactory jobBuilderFactory;

    public final StepBuilderFactory stepBuilderFactory;

    public BatchConfiguration(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;

    }

    @Bean
    public JobParametersValidator jobParametersValidator() {
        return new IndexJobParametersValidator();
    }

    @Bean
    public JobParametersValidator compositeJobParametersValidator() {
        CompositeJobParametersValidator bean = new CompositeJobParametersValidator();
        bean.setValidators(Collections.singletonList(jobParametersValidator()));
        return bean;
    }

    @Bean
    public ItemProcessor<IndexData, IndexData> itemProcessor() {
        return new IndexItemProcessor();
    }

    @Bean
    public ItemReader<IndexData> itemReader() {
        return new IndexItemReader();
    }

    @Bean
    public MongoItemWriter<IndexData> writer(MongoTemplate mongoTemplate) {
        return new MongoItemWriterBuilder<IndexData>().template(mongoTemplate).collection("jones_index")
                .build();
    }


    /**
     * step declaration
     * @return {@link Step}
     */
    @Bean
    public Step indexStep(MongoItemWriter<IndexData> itemWriter) {
        return stepBuilderFactory.get("indexStep")
                .<IndexData, IndexData>chunk(50)
                .reader(itemReader())
                .processor(itemProcessor())
                .writer(itemWriter)
                .taskExecutor(new SimpleAsyncTaskExecutor())
                .build();
    }



    /**
     * job declaration
     * @param listener {@link JobCompletionListener}
     * @return {@link Job}
     */
    @Bean
    public Job indexJob(JobCompletionListener listener, Step indexStep) {
        return jobBuilderFactory.get("indexJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(indexStep)
                .end()
                .validator(compositeJobParametersValidator())
                .build();
    }

    @Bean(name = "myJobLauncher")
    public JobLauncher simpleJobLauncher(JobRepository jobRepository) throws Exception {
        SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
        jobLauncher.setJobRepository(jobRepository);
        jobLauncher.setTaskExecutor(new SimpleAsyncTaskExecutor());
        jobLauncher.afterPropertiesSet();
        return jobLauncher;
    }

}
