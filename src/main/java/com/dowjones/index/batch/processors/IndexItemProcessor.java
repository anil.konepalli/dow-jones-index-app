package com.dowjones.index.batch.processors;

import com.dowjones.index.domain.IndexData;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemProcessor;


@Slf4j
public class IndexItemProcessor  implements ItemProcessor<IndexData, IndexData>, StepExecutionListener {


    /**
     * {@inheritDoc}
     */
    @Override
    public IndexData process(IndexData item) {
        return item;
    }

    @Override
    public void beforeStep(StepExecution stepExecution) {
        /* Nothing to do before */
    }

    @SneakyThrows
    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        String jobId = stepExecution.getJobParameters().getString("jobId");
        if(stepExecution.getStatus() == BatchStatus.COMPLETED) {
            log.info("Processing step completed {}", jobId);
        }
        return null;
    }

}
