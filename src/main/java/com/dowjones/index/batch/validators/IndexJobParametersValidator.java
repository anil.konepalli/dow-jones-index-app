package com.dowjones.index.batch.validators;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;

public class IndexJobParametersValidator implements org.springframework.batch.core.JobParametersValidator {

	@Override
	public void validate(final JobParameters jobParameters) throws JobParametersInvalidException {
			// Need to write the validations for Job Params
	}

}
