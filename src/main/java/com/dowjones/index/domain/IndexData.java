package com.dowjones.index.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Document(collection = "#{@environment.getProperty('mongodb.collection.name')}")
@Builder
public class IndexData {
    @Id
    private String id;
    private int quarter;
    @Indexed(unique = false)
    private String stock;
    private LocalDate date;
    private Double open;
    private Double high;
    private Double low;
    private Double close;
    private Long volume;
    private Double percentChangePrice;
    private Double percentChangeVolumeLastWeek;
    private Long previousWeeksVolume;
    private Double nextWeekOpen;
    private Double nextWeekClose;
    private Double percentageChangeNextWeeksPrice;
    private int daysToNextDivident;
    private Double percentReturnNextDivident;
    private Date createdTimestamp;

}