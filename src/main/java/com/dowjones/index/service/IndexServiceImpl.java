package com.dowjones.index.service;

import com.dowjones.index.batch.IndexJobLauncher;
import com.dowjones.index.domain.IndexData;
import com.dowjones.index.dto.IndexDataRequest;
import com.dowjones.index.dto.IndexDataResponse;
import com.dowjones.index.exceptions.FileUploadException;
import com.dowjones.index.exceptions.ResourceNotFoundException;
import com.dowjones.index.repository.IndexRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class IndexServiceImpl {

    private final IndexRepository indexRepository;

    private final IndexJobLauncher indexJobLauncher;

    private FilesStorageServiceImpl filesStorageService;

    /**
     * This method is used to fetch all the records that exist with the stock tiker.
     * @param stock
     * @return List<IndexDataResponse>
     * @throws ResourceNotFoundException
     */
    public List<IndexDataResponse> findByStock(String stock) throws ResourceNotFoundException {
        Optional<List<IndexData>> optionalIndexData = indexRepository.findByStock(stock);
        if(!optionalIndexData.isPresent() || optionalIndexData.get().isEmpty())
            throw  new ResourceNotFoundException("Data Not Found!");
        return optionalIndexData.get().stream().map(this::mapToResponse).collect(Collectors.toList());
    }

    /**
     * The idea to make the quarter optional is an assumption from the requirement.
     * Requirement says AA should return 12 records, that is possible only from first quarter.
     * For second qurter we have more than 12 records hence we defaulted to quarter 1
     * But provided option to pass the quarter.
     * If quarter is provided as 0 then it will return all quarters data.
     * @param stock
     * @param quarter
     * @return
     * @throws ResourceNotFoundException
     */
    public List<IndexDataResponse> findByStockAndQuarter(String stock, Optional<Integer> quarter) throws ResourceNotFoundException {
        Integer defaultQuarter = quarter.isPresent() ? quarter.get() : 1;
         if (defaultQuarter.intValue() == 0) {
                return findByStock(stock);
        } else {
             Optional<List<IndexData>> optionalIndexData = indexRepository.findByStockAndQuarter(stock, defaultQuarter);
            if (!optionalIndexData.isPresent() || optionalIndexData.get().isEmpty()) {
                throw new ResourceNotFoundException("Data Not Found!");
            }
             return optionalIndexData.get().stream().map(this::mapToResponse).collect(Collectors.toList());
         }
    }

    /**
     * This method will help us save the data and won;t be buplicated.
     * As per the index, we have weekly data.
     * The unique combination is quarter, stock ticker, Date.
     * If the combination exists it will upsert else it will create a new record.
     * @param indexDataRequest
     * @return
     */
    public IndexDataResponse save(IndexDataRequest indexDataRequest) {
        IndexData indexData=new IndexData();
        BeanUtils.copyProperties(indexDataRequest,indexData);
        indexData.setDate(this.setDate(indexDataRequest.getDate()));
        IndexData newIndexData = indexRepository.updateUsingFindAndReplace(indexData);
        log.info("IndexData is saved");
        return this.mapToResponse(newIndexData);
    }

    /**
     * File will be saved and processed asynchronously by Spring batch process.
     * TODO : We need to save the file with timestamp so that if multiple users uploads the same file, it won;t break.
     * @param file
     * @throws FileUploadException
     */
    public void uploadFile(MultipartFile file) throws FileUploadException {

        log.info("");
        try {
            filesStorageService.save(file);
            indexJobLauncher.launchFileToJob(file.getOriginalFilename());
        } catch (Exception e) {
            throw new FileUploadException(e.getMessage());
        }
        log.info("Upload");
    }


    private IndexDataResponse mapToResponse(IndexData indexData) {
        IndexDataResponse response=new IndexDataResponse();
        BeanUtils.copyProperties(indexData,response);
        return  response;
    }

    public LocalDate setDate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy");
        return LocalDate.parse(date, formatter);
    }
}