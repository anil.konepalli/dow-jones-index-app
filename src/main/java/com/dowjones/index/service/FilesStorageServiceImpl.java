package com.dowjones.index.service;

import com.dowjones.index.exceptions.ApplicationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
@Slf4j
public class FilesStorageServiceImpl implements FilesStorageService{

    Path root;

    @Value("${index.csv.processingfolder}")
    private String processingDir;


    @Override
    public void init() throws ApplicationException {
        try {
            log.info(new ClassPathResource(processingDir).getPath());
            root = Paths.get(new ClassPathResource(processingDir).getPath());
            Files.createDirectories(root);
        } catch (IOException e) {
            throw new ApplicationException("Could not initialize folder for upload!");
        }
    }

    @Override
    public void save(MultipartFile file) throws ApplicationException {
        try {
            this.init();
            Files.copy(file.getInputStream(),Paths.get(new ClassPathResource(processingDir+file.getOriginalFilename()).getPath()));
        }
        catch (FileAlreadyExistsException e) {
            deleteAll();
            throw new ApplicationException("A file of that name already exists.");
        }
        catch (Exception e) {
            throw new ApplicationException(e.getMessage());
        }
    }

    @Override
    public Resource load(String filename) throws ApplicationException {
        try {
            Path file = root.resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new ApplicationException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new ApplicationException("Error: " + e.getMessage());
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(root.toFile());
        log.info("File gets deleted {}",root.getFileName());
    }

}
