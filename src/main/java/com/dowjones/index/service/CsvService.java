package com.dowjones.index.service;

import com.dowjones.index.domain.IndexData;
import com.dowjones.index.exceptions.FileParsingException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class CsvService {

    public static final String TYPE = "text/csv";

    /**
     * Doing basic check but we can perform more validation to decide either to save or reject the file.
     * Headers validation
     * and Data validation if it is empty.
     * @param file
     * @return
     */
    public boolean hasCSVFormat(MultipartFile file) {
        log.info("File type uploaded {}", file.getContentType());
        return TYPE.equals(file.getContentType());
    }

    public List<IndexData> convertCsvtoIndexData(InputStream is) throws FileParsingException {
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
             CSVParser csvParser = new CSVParser(fileReader,
                     CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim())) {

            List<IndexData> indexDataList = new ArrayList<>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();
            for (CSVRecord csvRecord : csvRecords) {
                indexDataList.add(this.csvToIndexData(csvRecord));
            }

            return indexDataList;
        } catch (IOException e) {
            throw new FileParsingException("fail to parse CSV file: " + e.getMessage());
        }
    }

    @SneakyThrows
    private IndexData csvToIndexData(CSVRecord csvRecord) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy");

        return IndexData.builder()
                .quarter(Integer.parseInt(csvRecord.get("quarter")))
                .stock(csvRecord.get("stock"))
                .date(LocalDate.parse(csvRecord.get("date"), formatter))
                .open(convertStringToDouble((csvRecord.get("open"))))
                .high(convertStringToDouble(csvRecord.get("high")))
                .low(convertStringToDouble(csvRecord.get("low")))
                .close(convertStringToDouble(csvRecord.get("close")))
                .volume(convertStringToLong(csvRecord.get("volume")))
                .percentChangePrice(convertStringToDouble(csvRecord.get("percent_change_price")))
                .percentChangeVolumeLastWeek(convertStringToDouble(csvRecord.get("percent_change_volume_over_last_wk")))
                .previousWeeksVolume(convertStringToLong(csvRecord.get("previous_weeks_volume")))
                .nextWeekOpen(convertStringToDouble(csvRecord.get("next_weeks_open")))
                .nextWeekClose(convertStringToDouble(csvRecord.get("next_weeks_close")))
                .percentageChangeNextWeeksPrice(convertStringToDouble(csvRecord.get("percent_change_next_weeks_price")))
                .daysToNextDivident(convertStringToInt(csvRecord.get("days_to_next_dividend")))
                .percentReturnNextDivident(convertStringToDouble(csvRecord.get("percent_return_next_dividend")))
                .build();
    }

    private Double convertStringToDouble(String data) {
         if(StringUtils.isBlank(data)) {
            return 0.0;
         }
        return Double.valueOf(data.contains("$") ?   data.substring(1) : data);
    }

    private Long convertStringToLong(String data) {
        return StringUtils.isNotBlank(data) ? Long.valueOf(data) : 0L;
    }


    private Integer convertStringToInt(String data) {
        return StringUtils.isNotBlank(data) ? Integer.valueOf(data) : 0;
    }

}
