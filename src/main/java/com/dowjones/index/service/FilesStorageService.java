package com.dowjones.index.service;

import com.dowjones.index.exceptions.ApplicationException;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FilesStorageService {
    public void init() throws ApplicationException;

    public void save(MultipartFile file) throws ApplicationException;

    public Resource load(String filename) throws ApplicationException;

    public void deleteAll();

}
