package com.dowjones.index.dto;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class IndexDataRequest {

    private int quarter;
    private String stock;
    private String date;
    private Double open;
    private Double high;
    private Double low;
    private Double close;
    private Long volume;
    private Double percentChangePrice;
    private Double percentChangeVolumeLastWeek;
    private Long previousWeeksVolume;
    private Double nextWeekOpen;
    private Double nextWeekClose;
    private Double percentageChangeNextWeeksPrice;
    private int daysToNextDivident;
    private Double percentReturnNextDivident;
}
