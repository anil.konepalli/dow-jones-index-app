package com.dowjones.index.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IndexDataResponse {

    private String id;
    private int quarter;
    private String stock;
    private LocalDate date;
    private Double open;
    private Double high;
    private Double low;
    private Double close;
    private Long volume;
    private Double percentChangePrice;
    private Double percentChangeVolumeLastWeek;
    private Long previousWeeksVolume;
    private Double nextWeekOpen;
    private Double nextWeekClose;
    private Double percentageChangeNextWeeksPrice;
    private int daysToNextDivident;
    private Double percentReturnNextDivident;
}