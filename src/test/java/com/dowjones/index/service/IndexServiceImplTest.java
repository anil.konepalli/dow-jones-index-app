package com.dowjones.index.service;

import com.dowjones.index.batch.IndexJobLauncher;
import com.dowjones.index.domain.IndexData;
import com.dowjones.index.dto.IndexDataRequest;
import com.dowjones.index.dto.IndexDataResponse;
import com.dowjones.index.exceptions.ResourceNotFoundException;
import com.dowjones.index.repository.CustomIndexRepositoryImpl;
import com.dowjones.index.repository.IndexRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@ExtendWith(MockitoExtension.class)
class IndexServiceImplTest {

    @Mock
    IndexRepository indexRepository;
    @Mock
    CustomIndexRepositoryImpl customIndexRepositoryImpl;
    @Mock
    IndexJobLauncher indexJobLauncher;
    @Mock
    FilesStorageServiceImpl filesStorageService;

    @InjectMocks
    IndexServiceImpl indexService;

    @BeforeEach
    public void setUp() {
//        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Should find stock by Stock ticker")
    void shouldFindStockByStock() throws ResourceNotFoundException {
        Mockito.when(indexRepository.findByStock(Mockito.anyString())).thenReturn(getMockIndexDataList());
        assertThat(indexService.findByStock("AA").get(0).getStock()).isEqualTo("AA");
    }

    @Test
    @DisplayName("Should return empty stock by Stock ticker Not matching")
    void shouldThrowNotFoundExceptionFindStockByStock()  {
       Mockito.lenient().when(indexRepository.findByStock(Mockito.anyString())).thenReturn(Optional.of(Collections.emptyList()));
       assertThatExceptionOfType(ResourceNotFoundException.class).isThrownBy(
               () ->  { indexService.findByStock("AA"); }
            );
    }

    @Test
    @DisplayName("Should return index data by using stock and quarter")
    void shouldFindIndexDataByStockAndQuarter() throws ResourceNotFoundException {
        Mockito.lenient().when(indexRepository.findByStockAndQuarter(Mockito.any(), Mockito.any())).thenReturn(getMockIndexDataList());
        List<IndexDataResponse> indexDataResponse = indexService.findByStockAndQuarter("AA", Optional.of(1));
        assertThat(indexDataResponse.get(0).getStock()).isEqualTo("AA");

    }

    @Test
    @DisplayName("Should return index data by using stock and quarter try with param 2")
    void shouldFindIndexDataByStockAndQuarterWithDifferntParams() throws ResourceNotFoundException {
        Mockito.when(indexRepository.findByStockAndQuarter(Mockito.any(), Mockito.any())).thenReturn(getMockQuarterTwoIndexDataList());
        List<IndexDataResponse> indexDataResponse = indexService.findByStockAndQuarter("AA", Optional.of(2));
        assertThat(indexDataResponse.get(0).getQuarter()).isEqualTo(2);
    }

    @Test
    @DisplayName("Should return index data by using stock and quarter and param 0 to return all")
    void shouldFindAllIndexDataByStockAndQuarter() throws ResourceNotFoundException {
        Mockito.when(indexRepository.findByStock(Mockito.anyString())).thenReturn(getMockIndexDataList());
        List<IndexDataResponse> indexDataResponse = indexService.findByStockAndQuarter("AA", Optional.of(0));
        assertThat(indexDataResponse).hasSize(2);
    }

    @Test
    @DisplayName("Should return index data by using stock and quarter and param 0 to return all")
    void shouldSaveIndexData() {
        Mockito.when(indexRepository.updateUsingFindAndReplace(Mockito.any(IndexData.class))).thenReturn(getMockIndexData());
        IndexDataRequest indexData = IndexDataRequest.builder()
                .stock("AA")
                .date("1/01/2022")
                .quarter(1).build();
        IndexDataResponse indexDataResponse = indexService.save(indexData);
        assertThat(indexDataResponse.getStock()).isEqualTo(indexData.getStock());
    }


    private IndexData getMockIndexData() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy");
        IndexData indexData = IndexData.builder()
                .stock("AA")
                .date(LocalDate.parse("1/01/2022", formatter))
                .quarter(1).build();
        return indexData;
    }

    private Optional<List<IndexData>> getMockIndexDataList() {
        IndexData indexData1 = IndexData.builder()
                .stock("AA")
                .quarter(1).build();
        IndexData indexData2= IndexData.builder()
                .stock("AA")
                .quarter(2).build();
        return Optional.of(Arrays.asList(indexData1,indexData2));
    }

    private Optional<List<IndexData>> getMockQuarterTwoIndexDataList() {
        IndexData indexData= IndexData.builder()
                .stock("AA")
                .quarter(2).build();
        return Optional.of(Arrays.asList(indexData));
    }

    private Optional<List<IndexData>> getMockIndexDataEmptyList() {
        return Optional.of(Collections.emptyList());
    }
}