package com.dowjones.index.repository;

import com.dowjones.index.BaseTest;
import com.dowjones.index.domain.IndexData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

@DataMongoTest
class IndexRepositoryTest extends BaseTest {

    @Autowired
    IndexRepository indexRepository;

    List<IndexData> indexDataList= new ArrayList<>();

    @BeforeEach
    public void setUp() {
        indexRepository.deleteAll();
        IntStream.range(0,10).forEach( i-> {
            IndexData indexData = IndexData.builder()
                    .stock("Test").quarter(i <5 ? 1 : 2)
                    .date(LocalDate.of(2022, 01, 01).plusDays(i*7))
                    .build();
            indexData = indexRepository.save(indexData);
            indexDataList.add(indexData);
        });
    }

    @Test
    void shouldFindStockByStockName() {
        assertThat(indexRepository.findByStock("Test").get().size())
                .usingRecursiveComparison()
                .ignoringActualNullFields()
                .isEqualTo(10);
    }

    @Test
    void shouldFindStockByStockNameAndQuarter() {
        assertThat(indexRepository.findByStockAndQuarter("Test",1).get().size())
                .usingRecursiveComparison()
                .ignoringActualNullFields()
                .isEqualTo(5);
    }

    @Test
    void shouldUpdateUsingFindAndReplace() {
        IndexData indexData = indexDataList.get(0);
        indexData.setVolume(100L);
        IndexData returnedData = indexRepository.updateUsingFindAndReplace(indexData);
        assertThat(returnedData.getVolume()).isEqualTo(100L);
    }

    @Test
    void shouldUpsertUsingFindAndReplace() {
        IndexData indexData = indexDataList.get(0);
        indexData.setId(null);
        indexData.setStock("ABC");
        indexData.setVolume(100L);
        IndexData returnedData = indexRepository.updateUsingFindAndReplace(indexData);
        assertThat(returnedData.getVolume()).isEqualTo(100L);
        assertThat(returnedData.getStock()).isEqualTo("ABC");
    }


}