package com.dowjones.index;

import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;

public class BaseTest {
    @Container
    static MongoDBContainer mongoDBContainer= new MongoDBContainer("mongo:latest");

    static {
        mongoDBContainer.start();
    }
}
