package com.dowjones.index.controller;

import com.dowjones.index.dto.IndexDataRequest;
import com.dowjones.index.dto.IndexDataResponse;
import com.dowjones.index.service.CsvService;
import com.dowjones.index.service.IndexServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.testcontainers.shaded.org.hamcrest.Matchers;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@WebMvcTest(IndexController.class)
class IndexControllerTest {

    @MockBean
    private IndexServiceImpl indexService;

    @MockBean
    private CsvService csvService;

    @Autowired
    private MockMvc mockMvc;


    @Test
    @DisplayName("Basic status 200 for get Index Data API")
    void shouldRespond200() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/index").param("stock", "AA"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    @DisplayName("should get all the index data by Stock Name default is 1 ")
    void shouldGetIndexData() throws Exception {
        Mockito.when(indexService.findByStockAndQuarter(Mockito.anyString(),Mockito.any())).thenReturn(getMockIndexDataList());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/index").param("stock", "AA"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(mvcResult ->  MockMvcResultMatchers.jsonPath("$.size()", Matchers.is(2)));
    }

    @Test
    @DisplayName("Should save Index ")
    void shouldSaveIndexData() throws Exception {
        Mockito.when(indexService.save(Mockito.any())).thenReturn(getIndexDataResponse());
        mockMvc.perform(MockMvcRequestBuilders.post("/api/index")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getIndexDatRequestAsString()))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(mvcResult -> {
                    MockMvcResultMatchers.jsonPath("$.size()", Matchers.is(2));
                    MockMvcResultMatchers.jsonPath("$.[0].name", Matchers.is("AA"));

                });
    }

    private List<IndexDataResponse> getMockIndexDataList() {
        IndexDataResponse indexData1 = IndexDataResponse.builder()
                .stock("AA")
                .quarter(1).build();
        IndexDataResponse indexData2 = IndexDataResponse.builder()
                .stock("AA")
                .quarter(2).build();
        return Arrays.asList(indexData1,indexData2);
    }


    private IndexDataRequest getIndexDatRequest() {
        IndexDataRequest indexDataRequest = IndexDataRequest.builder()
                .stock("AA")
                .volume(1000L)
                .date("01/01/2022")
                .quarter(1).build();
        return indexDataRequest;
    }

    private IndexDataResponse getIndexDataResponse() {
        return IndexDataResponse.builder()
                .stock("AA")
                .volume(1000L)
                .date(LocalDate.of(2022,01,01))
                .quarter(1).build();
    }

    private String getIndexDatRequestAsString() {
       return "{" +
               "\"quarter\" : 1,\n" +
               "\"stock\" : \"AA\",\n" +
               "\"open\" : 15.82,\n" +
               "\"high\" : 16.72,\n" +
               "\"low\" : 15.78,\n" +
               "\"close\" : 16.42,\n" +
               "\"volume\" : 239655616,\n" +
               "\"percent_change_price\" : 3.79267,\n" +
               "\"next_weeks_open\": 16.71,\n" +
               "\"next_weeks_close\" : 15.97,\n" +
               "\"percent_change_next_weeks_price\" : -4.42849,\n" +
               "\"days_to_next_dividend\" : 26,\n" +
               "\"percent_return_next_dividend\" : 0.182704\n" +
               "}";
    }


}