package com.dowjones.index.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.testcontainers.shaded.org.hamcrest.Matchers;


@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@TestPropertySource(locations = "application.properties")
public class IndexControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void get404IfDataNotFoundForGivenStockTicker()
            throws Exception {

        mvc.perform(MockMvcRequestBuilders.get("/api/index").param("stock", "ABCD"))
                .andExpect(MockMvcResultMatchers.status().is(404));

    }

    @Test
    public void saveIndexDataForGivenStockTicker()
            throws Exception {

        mvc.perform(MockMvcRequestBuilders.post("/api/index")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(getIndexDatRequestAsString()))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(mvcResult -> {
                    MockMvcResultMatchers.jsonPath("$.size()", Matchers.is(2));
                    MockMvcResultMatchers.jsonPath("$.[0].name", Matchers.is("AA"));

                });
    }

    @Test
    public void getIndexDatForGivenStockTicker()
            throws Exception {

        mvc.perform(MockMvcRequestBuilders.get("/api/index").param("stock", "AA"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(mvcResult ->  MockMvcResultMatchers.jsonPath("$.size()", Matchers.is(1)));

    }


    private String getIndexDatRequestAsString() {
        return "{" +
                "\"quarter\" : 1,\n" +
                "\"stock\" : \"AA\",\n" +
                "\"open\" : 15.82,\n" +
                "\"high\" : 16.72,\n" +
                "\"low\" : 15.78,\n" +
                "\"close\" : 16.42,\n" +
                "\"volume\" : 239655616,\n" +
                "\"percent_change_price\" : 3.79267,\n" +
                "\"next_weeks_open\": 16.71,\n" +
                "\"next_weeks_close\" : 15.97,\n" +
                "\"percent_change_next_weeks_price\" : -4.42849,\n" +
                "\"days_to_next_dividend\" : 26,\n" +
                "\"percent_return_next_dividend\" : 0.182704\n" +
                "}";
    }


}